<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
        return view('home');
    }

    public function user()
    {
        return view('user.index');
    }

    public function register(Request $request)
    {
        $user = User::create([
            'name' => $request->business_name,
            'email' => $request->email,
            'password' => bcrypt($password),
        ]);

        $user->api_token = hash('sha256', $password);

        $user->save();

        return view('home');


    }

    public function padron()
    {
        return view('padron.index');
    }
}
