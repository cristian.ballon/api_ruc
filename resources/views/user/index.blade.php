@extends('layouts.app')

@section('content')

    <user-info :user="{{ json_encode( auth()->user() ) }}" ></user-info>
   
@endsection
